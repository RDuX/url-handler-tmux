#! /usr/bin/env bash

# Regex to look for common URL patterns.
urlregex="(((http|https)://|www\\.)[a-zA-Z0-9.]+[:]?[a-zA-Z0-9./@$&%?~\\+\\(\\)!,:#=_-]+)"

# Capture input
input=$(tmux capture-pane -Jp)

# Check and clean input from weechat TUI client
if $(echo -e "$input" | grep -E '^.+│.+\|\s.*\s│.*$' > /dev/null); then
  input=$(echo -e "$input" |         # Print the original input
    sed -r 's/^(\s+|.+│.+\|\s)//g' | # Strip the channel bar and user info
    sed -r 's/\s│.*//g' |            # Strip the trailing bar & users
    tr -d '\n')                      # Remove newlines in order to handle wrapped urls
fi

# Extract URLs from the currently visible Tmux buffer
urls=$(echo -e "$input" |			    # Print the modified input
  grep -aEo "$urlregex" |         # Grep only URLs as defined above.
  uniq |                          # Remove neighboring duplicates.
  sed 's|^www\.|http://www\.|g' | # Prefix url starting with 'www.' with http://.
  tac)                            # Reverse the link order.

# If no URLs were found exit.
[ -z $urls ] && exit

# Show the 10 newest links in wofi for interactive selection.
selected=$(echo "$urls" | rofi -dmenu -i -p 'Open URL' -l 10)

# If a URL is selected open it.
if [[ $selected != "" ]]; then
  # Find the current directory path.
  CURRENT_DIR=$(dirname $0)

  # Check if app.sh is present and if it's executable.
  if [[ -x "$CURRENT_DIR/app.sh" ]]; then
    setsid sh -c "$CURRENT_DIR/app.sh '$selected'"
  else
    setsid $BROWSER "$selected" >/dev/null 2>&1 &
  fi
fi
